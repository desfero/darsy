namespace Darsy {
  export function Controller(name: string) {
      return function (target: Function) {
          Darsy["Controllers"] = Darsy["Controllers"] || new Array<Darsy.Interfaces.IComponent>();
          Darsy["Controllers"].push({name, target})
      }
  }

  export function Service(name: string) {
      return function (target: Function) {
          Darsy["Services"] = Darsy["Services"] || new Array<Darsy.Interfaces.IComponent>();
          Darsy["Services"].push({name, target})
      }
  }

  export function Directive(name: string) {
      return function (target: Function) {
          Darsy["Directives"] = Darsy["Directives"] || new Array<Darsy.Interfaces.IComponent>();
          Darsy["Directives"].push({name, target})
      }
  }


  export function Filter(name: string) {
      return function (target: Function) {
          Darsy["Filters"] = Darsy["Filters"] || new Array<Darsy.Interfaces.IComponent>();
          Darsy["Filters"].push({name, target})
      }
  }
}
