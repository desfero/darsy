module Darsy.Interfaces {
    export interface IComponent {
        name: string;
        target: Function;
    }
}
