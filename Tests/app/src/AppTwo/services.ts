'use strict';

namespace AppTwo.Services {
    @Darsy.Service("MyService")
    export class MyService implements IService {
        private meaningOfLife = 42;
        constructor () {
        }
        someMethod () {
            return 'Meaning of life is ' + this.meaningOfLife;
        }
    }
}
