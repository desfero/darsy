var Darsy;
(function (Darsy) {
    function Controller(name) {
        return function (target) {
            Darsy["Controllers"] = Darsy["Controllers"] || new Array();
            Darsy["Controllers"].push({ name: name, target: target });
        };
    }
    Darsy.Controller = Controller;
    function Service(name) {
        return function (target) {
            Darsy["Services"] = Darsy["Services"] || new Array();
            Darsy["Services"].push({ name: name, target: target });
        };
    }
    Darsy.Service = Service;
    function Directive(name) {
        return function (target) {
            Darsy["Directives"] = Darsy["Directives"] || new Array();
            Darsy["Directives"].push({ name: name, target: target });
        };
    }
    Darsy.Directive = Directive;
    function Filter(name) {
        return function (target) {
            Darsy["Filters"] = Darsy["Filters"] || new Array();
            Darsy["Filters"].push({ name: name, target: target });
        };
    }
    Darsy.Filter = Filter;
})(Darsy || (Darsy = {}));
var Darsy;
(function (Darsy) {
    var Helpers;
    (function (Helpers) {
        function isUrlValid(str) {
            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
            if (!regex.test(str)) {
                return false;
            }
            else {
                return true;
            }
        }
        Helpers.isUrlValid = isUrlValid;
        var Angular;
        (function (Angular) {
            var $injector = angular.injector(['ng']);
            Angular.$http = $injector.get('$http');
            Angular.$q = $injector.get('$q');
        })(Angular = Helpers.Angular || (Helpers.Angular = {}));
    })(Helpers = Darsy.Helpers || (Darsy.Helpers = {}));
})(Darsy || (Darsy = {}));
var Darsy;
(function (Darsy) {
    var Injector = (function () {
        function Injector(appName, externalModules) {
            var _this = this;
            this.inject = function (config) {
                _this.registerComponents();
                config(_this.app);
                angular.element(document).ready(function () {
                    angular.bootstrap(document, [_this.app.name]);
                });
            };
            this.registerControllers = function (dependencies) {
                var controllers = Darsy["Controllers"];
                if (Object.prototype.toString.call(controllers) === '[object Array]') {
                    for (var _i = 0; _i < controllers.length; _i++) {
                        var component = controllers[_i];
                        if (typeof dependencies[component.name] === "undefined") {
                            throw new Error("Controller " + component.name + " doesn't have dependencies defined");
                        }
                        var fullControllerName = _this.app.name + "." + "Controllers" + "." + component.name;
                        component.target.$inject = dependencies[component.name];
                        _this.app.controller(fullControllerName, component.target);
                    }
                }
            };
            this.registerDirectives = function (dependencies) {
                var directives = Darsy["Directives"];
                if (Object.prototype.toString.call(directives) === '[object Array]') {
                    for (var _i = 0; _i < directives.length; _i++) {
                        var component = directives[_i];
                        if (typeof dependencies[component.name] === "undefined") {
                            throw new Error("Directive " + component.name + " doesn't have dependencies defined");
                        }
                        _this.app.directive(component.name, _this.directiveFactory(component.target, dependencies[component.name]));
                    }
                }
            };
            this.registerServices = function (dependencies) {
                var services = Darsy["Services"];
                for (var _i = 0; _i < services.length; _i++) {
                    var component = services[_i];
                    if (Object.prototype.toString.call(services) === '[object Array]') {
                        if (typeof dependencies[component.name] === "undefined") {
                            throw new Error("Service " + component.name + " doesn't have dependencies defined");
                        }
                        var fullServiceName = _this.app.name + "." + "Services" + "." + component.name;
                        component.target.$inject = dependencies[component.name];
                        _this.app.service(fullServiceName, component.target);
                    }
                }
            };
            this.registerFilters = function (dependencies) {
                var filters = Darsy["Filters"];
                if (Object.prototype.toString.call(filters) === '[object Array]') {
                    for (var _i = 0; _i < filters.length; _i++) {
                        var component = filters[_i];
                        if (typeof dependencies[component.name] === "undefined") {
                            throw new Error("Filter " + component.name + " doesn't have dependencies defined");
                        }
                        _this.app.filter(component.name, _this.filterFactory(component.target, dependencies[component.name]));
                    }
                }
            };
            this.registerComponents = function () {
                var dependencies = _this.dependencies;
                for (var componentName in dependencies) {
                    if (dependencies.hasOwnProperty(componentName)) {
                        switch (componentName) {
                            case "Controllers":
                                _this.registerControllers(dependencies[componentName]);
                                break;
                            case "Directives":
                                _this.registerDirectives(dependencies[componentName]);
                                break;
                            case "Services":
                                _this.registerServices(dependencies[componentName]);
                                break;
                            case "Filters":
                                _this.registerFilters(dependencies[componentName]);
                                break;
                            default:
                                throw new Error(componentName + " are not implemented");
                                break;
                        }
                    }
                }
            };
            this.directiveFactory = function (directive, dependencies) {
                var that = _this;
                var factory = function () {
                    return that.construct(directive, arguments);
                };
                factory.$inject = dependencies;
                return factory;
            };
            this.filterFactory = function (filter, dependencies) {
                var that = _this;
                var factory = function () {
                    return that.construct(filter, arguments).filter;
                };
                factory.$inject = dependencies;
                return factory;
            };
            this.app = angular.module(appName, externalModules);
        }
        Injector.prototype.initialize = function (dependencies) {
            var that = this;
            var $q = Darsy.Helpers.Angular.$q;
            return $q(function (resolve) {
                if (typeof dependencies === 'string') {
                    that.getDependenciesByAjax(dependencies).then(function (result) {
                        that.dependencies = result;
                        resolve();
                    });
                }
                else {
                    that.dependencies = dependencies;
                    resolve();
                }
            });
        };
        Injector.prototype.getDependenciesByAjax = function (url) {
            var $q = Darsy.Helpers.Angular.$q;
            return $q(function (resolve) {
                if (Darsy.Helpers.isUrlValid(url)) {
                    Darsy.Helpers.Angular.$http.get(url)
                        .then(function (response) {
                        resolve(response.data);
                    }, function (response) {
                        var text = url + ': ' + response.data;
                        throw new Error(text);
                    });
                }
                else {
                    throw new Error(url + ': invalid url address');
                }
            });
        };
        Injector.prototype.construct = function (constructor, args) {
            function F() {
                constructor.apply(this, args);
            }
            F.prototype = constructor.prototype;
            return new F();
        };
        return Injector;
    })();
    Darsy.Injector = Injector;
})(Darsy || (Darsy = {}));
//# sourceMappingURL=darsy.js.map