'use strict';
module AppTwo {
  export interface IController {}
  export interface IDirective {
    restrict: string;
    link($scope: ng.IScope, element: JQuery, attrs: ng.IAttributes): any;
  }
  export interface IFilter {
    filter (input: any, ...args: any[]): any;
  }
  export interface IService {}

  var darsy = new Darsy.Injector("AppTwo", ["ngRoute"]);
  darsy.initialize("http://localhost:8000/app/src/AppTwo/dependencies.json").then(function() {
    var app = darsy.inject(function(app) {
      // Url routing
      app.config(['$routeProvider',
      function routes($routeProvider) {
        $routeProvider
        .when('/', {
          templateUrl: 'src/AppTwo/views/MyView.html',
          controller: 'AppTwo.Controllers.MyController'
        })
        .otherwise({
          redirectTo: '/'
        });
      }
    ]);
  });
}, function(message){
  
});


}
