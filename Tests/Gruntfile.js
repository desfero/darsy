module.exports = function (grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    web_server: {
      options: {
        cors: true,
        port: 8000,
        nevercache: true,
        logRequests: true
      },
      foo: 'bar'    // For some reason an extra key with a non-object value is necessary
    },
    karma: {
      appone: {
        options: {
          configFile: './specs/AppOne/karma.conf.js',
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-web-server');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('tests', ['karma']);
};
