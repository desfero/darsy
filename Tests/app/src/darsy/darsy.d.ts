declare namespace Darsy {
    function Controller(name: string): (target: Function) => void;
    function Service(name: string): (target: Function) => void;
    function Directive(name: string): (target: Function) => void;
    function Filter(name: string): (target: Function) => void;
}
declare module Darsy.Helpers {
    function isUrlValid(str: string): boolean;
    module Angular {
        var $http: ng.IHttpService;
        var $q: ng.IQService;
    }
}
declare module Darsy.Interfaces {
    interface IComponent {
        name: string;
        target: Function;
    }
}
declare module Darsy {
    class Injector {
        private dependencies;
        private app;
        constructor(appName: string, externalModules: string[]);
        inject: (config: Function) => void;
        initialize(dependencies: Object | string): ng.IPromise<{}>;
        private getDependenciesByAjax(url);
        private registerControllers;
        private registerDirectives;
        private registerServices;
        private registerFilters;
        private registerComponents;
        private directiveFactory;
        private filterFactory;
        private construct(constructor, args);
    }
}
