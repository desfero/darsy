module Darsy {

  export class Injector {
    private dependencies: Object;
    private app: ng.IModule;

    public constructor(appName: string, externalModules: string[]) {
      this.app = angular.module(appName, externalModules);
    }

    public inject = (config: Function) : void =>  {
      this.registerComponents();

      config(this.app);

      angular.element(document).ready(() => {
        angular.bootstrap(document, [this.app.name]);
      });
    }

    public initialize(dependencies : Object | string) : ng.IPromise<{}> {
      var that : Darsy.Injector = this;
      var $q = Helpers.Angular.$q;
      return $q(function(resolve){
        if(typeof dependencies === 'string'){
          that.getDependenciesByAjax(dependencies).then(function(result) {
            that.dependencies = result;
            resolve();
          });
        }
        else {
          that.dependencies = dependencies;
          resolve();
        }
      });
    }

    private getDependenciesByAjax(url: string) : ng.IPromise<{}> {
      var $q = Helpers.Angular.$q;
      return $q(function(resolve) {
        if(Darsy.Helpers.isUrlValid(url)) {
          Helpers.Angular.$http.get(url)
          .then(function(response) {
            resolve(response.data);
          }, function(response) {
            var text = url + ': ' + response.data;
            throw new Error(text);
          });
        }
        else {
          throw new Error(url + ': invalid url address');
        }
      });
    }

    private registerControllers = (dependencies: any) => {
      var controllers = Darsy["Controllers"];
      if(Object.prototype.toString.call(controllers) === '[object Array]')
      {
        for (let component of controllers) {
          if (typeof dependencies[component.name] === "undefined") {
            throw new Error(`Controller ${component.name} doesn't have dependencies defined`);
          }

          const fullControllerName = this.app.name + "." + "Controllers" + "." + component.name;

          component.target.$inject = dependencies[component.name];
          this.app.controller(fullControllerName, component.target);
        }
      }
    }

    private registerDirectives = (dependencies: any) => {
      var directives : Array<Interfaces.IComponent> = Darsy["Directives"];
      if(Object.prototype.toString.call(directives) === '[object Array]') {
        for (let component of directives) {
          if (typeof dependencies[component.name] === "undefined") {
            throw new Error(`Directive ${component.name} doesn't have dependencies defined`);
          }

          this.app.directive(component.name, this.directiveFactory(component.target, dependencies[component.name]));
        }
      }
    }

    private registerServices = (dependencies: any) => {
      var services : Array<Interfaces.IComponent> = Darsy["Services"];
      for (let component of services) {
        if(Object.prototype.toString.call(services) === '[object Array]') {
          if (typeof dependencies[component.name] === "undefined") {
            throw new Error(`Service ${component.name} doesn't have dependencies defined`);
          }

          const fullServiceName = this.app.name + "." + "Services" + "." + component.name;

          component.target.$inject = dependencies[component.name];
          this.app.service(fullServiceName, component.target);
        }
      }
    }

    private registerFilters = (dependencies: any) => {
      var filters : Array<Interfaces.IComponent> = Darsy["Filters"];
      if(Object.prototype.toString.call(filters) === '[object Array]') {
        for (let component of filters) {
          if (typeof dependencies[component.name] === "undefined") {
            throw new Error(`Filter ${component.name} doesn't have dependencies defined`);
          }

          this.app.filter(component.name, this.filterFactory(component.target, dependencies[component.name]));
        }
      }
    }

    private registerComponents = () => {
      var dependencies = this.dependencies;
      for (let componentName in dependencies) {
        if (dependencies.hasOwnProperty(componentName)) {
          switch (componentName) {
            case "Controllers":
            this.registerControllers(dependencies[componentName]);
            break;
            case "Directives":
            this.registerDirectives(dependencies[componentName]);
            break;
            case "Services":
            this.registerServices(dependencies[componentName]);
            break;
            case "Filters":
            this.registerFilters(dependencies[componentName]);
            break;
            default:
            throw new Error(componentName + " are not implemented");
            break;
          }
        }
      }
    }

    private directiveFactory = (directive, dependencies): angular.IDirectiveFactory => {
      var that = this;
      var factory = function() {
        return that.construct(directive, arguments);
      }
      factory.$inject = dependencies;
      return factory;
    }

    private filterFactory = (filter, dependencies): angular.IDirectiveFactory => {
      var that = this;
      var factory = function() {
        return that.construct(filter, arguments).filter;
      }
      factory.$inject = dependencies;
      return factory;
    }

    private construct(constructor, args) {
      function F() : void {
        constructor.apply(this, args);
      }
      F.prototype = constructor.prototype;
      return new F();
    }
  }
}
