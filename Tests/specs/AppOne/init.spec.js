describe('Check if AppOne components are registered.', function() {
  beforeEach(function(done) {
    module('AppOne')

    setTimeout(function() {
      // wait for initializing angularjs components
      done();
   }, 1);
  });

  describe('Controllers:', function() {
    var $controller, exist;

    // Check if controler with given name exist
    exists = function(controllerName) {
      if(typeof window[controllerName] == 'function') {
        return true;
      }
      try {
        $controller(controllerName);
        return true;
      } catch (error) {
        return !(error instanceof TypeError);
      }
    };

    beforeEach(inject(function(_$controller_) {
      $controller = _$controller_;
    }));

    it('MyController should exist', function() {
      expect(exists("AppOne.Controllers.MyController")).toBeTruthy();
    });

     it('NotMyController should not exist', function() {
      expect(exists("AppOne.Controllers.NotMyController")).toBeFalsy();
    });
  });

  describe('Directives:', function() {
    var $injector, exists;

    // Check if directive with given name exist
    exists = function(directiveName) {
      directiveName += "Directive";
      return $injector.has(directiveName);
    };

    beforeEach(inject(function(_$injector_) {
      $injector = _$injector_;
    }));

    it('myDirective should exist', function() {
      expect(exists("myDirective")).toBeTruthy();
    });

    it('notMyDirective should not exist', function() {
      expect(exists("notMyDirective")).toBeFalsy();
    });
  });

  describe('Services:', function() {
    var $injector, exists;

    // Check if directive with given name exist
    exists = function(serviceName) {
      return $injector.has(serviceName);
    };

    beforeEach(inject(function(_$injector_) {
      $injector = _$injector_;
    }));

    it('MyService should exist', function() {
      expect(exists("AppOne.Services.MyService")).toBeTruthy();
    });

    it('NotMyService should not exist', function() {
      expect(exists("AppOne.Services.NotMyService")).toBeFalsy();
    });

    describe('Filters:', function() {
      var $injector, exists;

      // Check if directive with given name exist
      exists = function(filterName) {
        filterName += "Filter";
        return $injector.has(filterName);
      };

      beforeEach(inject(function(_$injector_) {
        $injector = _$injector_;
      }));

      it('RangeTo should exist', function() {
        expect(exists("RangeTo")).toBeTruthy();
      });

      it('Splice should exist', function() {
        expect(exists("Splice")).toBeTruthy();
      });

      it('NotMy should not exist', function() {
        expect(exists("NotMy")).toBeFalsy();
      });
    });
  });
});
