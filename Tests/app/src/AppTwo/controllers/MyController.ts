'use strict';
namespace AppTwo.Controllers {
    @Darsy.Controller("MyController")
    export class MyController implements IController {
        constructor (private $scope, private myService) {
            $scope.message = myService.someMethod();
        }
    }
}
