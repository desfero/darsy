'use strict';
var AppTwo;
(function (AppTwo) {
    var darsy = new Darsy.Injector("AppTwo", ["ngRoute"]);
    darsy.initialize("http://localhost:8000/app/src/AppTwo/dependencies.json").then(function () {
        var app = darsy.inject(function (app) {
            // Url routing
            app.config(['$routeProvider',
                function routes($routeProvider) {
                    $routeProvider
                        .when('/', {
                        templateUrl: 'src/AppTwo/views/MyView.html',
                        controller: 'AppTwo.Controllers.MyController'
                    })
                        .otherwise({
                        redirectTo: '/'
                    });
                }
            ]);
        });
    }, function (message) {
    });
})(AppTwo || (AppTwo = {}));
