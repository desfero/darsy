'use strict';
namespace AppOne.Controllers {
    @Darsy.Controller("MyController")
    export class MyController implements IController {
        constructor (private $scope, private myService) {
            $scope.message = myService.someMethod();
        }
    }
}
