'use strict';

namespace AppTwo.Filters {
  @Darsy.Filter("RangeTo")
  export class RangeTo implements IFilter {
    filter (start: number, end: number) {
      var out = [];
      for (var i = start; i < end; ++i) out.push(i)
      return out
    }
  }

  @Darsy.Filter("Splice")
  export class Splice implements IFilter {
    filter (input: Array<Object>, start: number, howMany: number) {
      return input.splice(start, howMany)
    }

  }
}
