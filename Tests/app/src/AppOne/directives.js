'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AppOne;
(function (AppOne) {
    var Directives;
    (function (Directives) {
        var myDirective = (function () {
            function myDirective(myService) {
                var _this = this;
                this.myService = myService;
                this.template = '<div></div>';
                this.restrict = 'E';
                this.link = function ($scope, element, attrs) {
                    var text = _this.myService.someMethod();
                    element.text('MyDirective: ' + text);
                };
            }
            myDirective = __decorate([
                Darsy.Directive("myDirective"), 
                __metadata('design:paramtypes', [])
            ], myDirective);
            return myDirective;
        })();
        Directives.myDirective = myDirective;
    })(Directives = AppOne.Directives || (AppOne.Directives = {}));
})(AppOne || (AppOne = {}));
