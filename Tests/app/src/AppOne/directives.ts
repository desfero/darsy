'use strict';

module AppOne.Directives {
  @Darsy.Directive("myDirective")
  export class myDirective implements IDirective {
    constructor (private myService) {

    }
    template = '<div></div>';
    restrict = 'E';
    link = ($scope, element: JQuery, attrs: ng.IAttributes) => {
      var text = this.myService.someMethod();
        element.text('MyDirective: ' + text);
    };
  }
}
