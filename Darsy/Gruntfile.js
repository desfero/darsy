module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Constants for the Gruntfile so we can easily change the path for
    // our environments.
    DESTINATION_PATH: 'dest/',
    SOURCE_PATH: 'src/',
    BASE_PATH: '',

    //
    uglify: {
      main: {
        files: {
          '<%= DESTINATION_PATH %>darsy.min.js' : ['<%= DESTINATION_PATH %>darsy.js']
        }
      }
    },
    //
    copy: {
      main: {
        files: [
          {expand: true, flatten: true, src: ['<%= DESTINATION_PATH %>*'], dest: '../Tests/app/src/darsy/'},
        ],
      },
    },
    //
    yuidoc: {
      compile: {
        name: '<%= pkg.name %>',
        description: '<%= pkg.description %>',
        version: '<%= pkg.version %>',
        url: '<%= pkg.homepage %>',
        options: {
          extension: '.ts',
          paths: '<%= SOURCE_PATH %>',
          outdir: '<%= BASE_PATH %>docs/'
        }
      }
    },
    // Wait for changes in '/dest' folder, and if something was changed run ready task
    watch: {
      dest: {
        files: ['<%= DESTINATION_PATH %>*'],
        tasks: ['ready'],
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-yuidoc');

  // Uglify and then copy all from '/dest' folder to Tests project
  grunt.registerTask('ready', ['uglify', 'copy']);
};
