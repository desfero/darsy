'use strict';
var AppOne;
(function (AppOne) {
    var dependencies = {
        "Controllers": {
            "MyController": ['$scope', 'AppOne.Services.MyService']
        },
        "Services": {
            "MyService": [],
        },
        "Directives": {
            "myDirective": ["AppOne.Services.MyService"],
        },
        "Filters": {
            "RangeTo": [],
            "Splice": []
        },
    };
    var darsy = new Darsy.Injector("AppOne", ["ngRoute"]);
    darsy.initialize(dependencies).then(function () {
        darsy.inject(function (app) {
            // Url routing
            app.config(['$routeProvider',
                function routes($routeProvider) {
                    $routeProvider
                        .when('/', {
                        templateUrl: 'src/AppOne/views/MyView.html',
                        controller: 'AppOne.Controllers.MyController'
                    })
                        .otherwise({
                        redirectTo: '/'
                    });
                }
            ]);
        });
    });
})(AppOne || (AppOne = {}));
