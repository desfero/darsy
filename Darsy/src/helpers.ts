module Darsy.Helpers {
  export function isUrlValid(str : string) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if(!regex.test(str)) {
      return false;
    } else {
      return true;
    }
  }

  export module Angular {
    var $injector = angular.injector(['ng']);

    export var $http : ng.IHttpService = <ng.IHttpService>$injector.get('$http');

    export var $q : ng.IQService = <ng.IQService>$injector.get('$q');
  }
}
